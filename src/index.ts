import { flatMap, fromFunction, Mapper, pipe, Predicate, SyncTransform, tap, throttle } from '@atlassian/cs-streams';
import { Tracer } from '@atlassian/koa-tracer';
import { Logger } from '@atlassian/logger';
import Metrics from '@atlassian/metrics';
import DynamoDBClient, { RequestOptions } from '@atlassian/yeoman-dynamodb';
import { PassThrough, Writable } from 'stream';

const THROTTLE = 5000;
const SCANLIMIT = 20;
export interface MigrationOptions extends RequestOptions {
  throttle?: number;
  scanLimit?: number;
}

export interface ScanQuery {
  FilterExpression?: string;
  ExpressionAttributeNames?: {
    [key: string]: string;
  };
  ExpressionAttributeValues?: AWS.DynamoDB.AttributeMap;
  IndexName?: string;
}

export interface UpdateQuery {
  Key: AWS.DynamoDB.AttributeMap;
  UpdateExpression: string;
  ExpressionAttributeValues: AWS.DynamoDB.AttributeMap;
  ReturnValues?: string;
}

/* Return data will be logged */
export type ScanResultProcessFunction = (scanedValue: ScanResult) => Promise<any>;

export interface ScanResult {
  LastEvaluatedKey?: AWS.DynamoDB.AttributeMap;
  Items?: AWS.DynamoDB.AttributeMap[];
  ConsumedCapacity?: AWS.DynamoDB.ConsumedCapacity;
  Count?: number;
  ScannedCount?: number;
}

export function actions(scan: (cursor?: any) => Promise<ScanResult>,
                        opt: MigrationOptions,
                        scanResultProcessor: ScanResultProcessFunction) {
  return pipe(
    fromCursor(scan, 'LastEvaluatedKey', undefined),
    repeatWhile(value => value.LastEvaluatedKey !== undefined),
    throttle(opt.throttle || THROTTLE),
    flatMap(async (scanedValue) => {
      return await scanResultProcessor(scanedValue);
    }),
    tap(r => {
      if (opt.log && r) {
        opt.log.info('Done with: ', r);
      }
    }),
  );
}

export const scanAction = (
  dynamoClient: DynamoDBClient,
  tableName: string,
  opt: MigrationOptions,
  additionalQuery: ScanQuery) => {
  return async (lastEvaluatedKey?: AWS.DynamoDB.DocumentClient.Key) => {
    let scanQuery: AWS.DynamoDB.DocumentClient.QueryInput = {
      TableName: tableName,
      ConsistentRead: true,
      Limit: opt.scanLimit || SCANLIMIT,
    };
    if (additionalQuery) {
      scanQuery = { ...scanQuery, ...additionalQuery };
    }
    if (lastEvaluatedKey) {
      scanQuery.ExclusiveStartKey = lastEvaluatedKey;
    }
    return await dynamoClient.scan(scanQuery, opt);
  };
};

export const updateAction = async (dynamoClient: DynamoDBClient,
                                   tableName: string, opt: MigrationOptions, query: UpdateQuery) => {
  const updateParams: AWS.DynamoDB.DocumentClient.UpdateItemInput = {
    TableName: tableName, ...query,
  };
  return await dynamoClient.update(updateParams, opt);
};

export const fromCursor = <T, K extends keyof T>(
  action: (cursor: T[K] | undefined) => Promise<T>,
  cursorProperty: K,
  initialCursor: T[K] | undefined,
): AsyncIterable<T> => {
  let currentCursor = initialCursor;

  return fromFunction(async () => {
    const result = await action(currentCursor);
    currentCursor = result[cursorProperty];
    return result;
  });
};

const streamEvent = (value: any) => `${JSON.stringify(value)}\n`;

export function repeatWhile<T>(predicate: Predicate<T>): Mapper<T, T> {
  return async function*(iterator) {
    for await (const item of iterator) {
      yield item;

      if (!predicate(item)) {
        return;
      }
    }
  };
}

const writeAsyncIterableToStream = async <T>(iterator: AsyncIterable<T>, stream: Writable) => {
  for await (const item of iterator) {
    stream.write(streamEvent(item));
  }
};

export function toStream<T>(log?: Logger): SyncTransform<AsyncIterable<T>, Writable> {
  return (iterator) => {
    const stream = new PassThrough();

    writeAsyncIterableToStream(iterator, stream).then(
      () => {
        stream.end();
      },
      (error) => {
        if (log) {
          log.error('Error occured, terminating the manipulation process', error);
        }
        stream.write(streamEvent(error));
        stream.end();
        throw new Error(error);
    });

    return stream;
  };
}

export interface DynamoConfig {
  apiVersion?: string;
  region: string;
  endpoint?: string;
  accessKeyId?: string;
  secretAccessKey?: string;
}

export interface DynamoDataManipulatorOptions {
  throttle?: number;
  scanLimit?: number;
  tracer?: Tracer;
  log?: Logger;
}

export default class DynamoDataManipulator {
  dynamoClient: DynamoDBClient;
  tableName: string;
  private opt: DynamoDataManipulatorOptions;

  constructor(dynamoConfig: DynamoConfig, tableName: string, metrics: Metrics, opt: DynamoDataManipulatorOptions) {
    this.tableName = tableName;
    /* Default throttle 5s to avoid reach read/write limit*/
    this.opt = opt;
    this.dynamoClient = new DynamoDBClient({
      metrics,
      apiVersion: dynamoConfig.apiVersion,
      endpoint: dynamoConfig.endpoint,
      region: dynamoConfig.region,
      accessKeyId: dynamoConfig.accessKeyId,
      secretAccessKey: dynamoConfig.secretAccessKey,
    });
  }

  updator = async (query: UpdateQuery) => {
    return await updateAction(this.dynamoClient, this.tableName, this.opt, query);
  }

  streamData(scanQuery: ScanQuery, scanResultProcessor: ScanResultProcessFunction) {
    const scanDB = scanAction(this.dynamoClient, this.tableName, this.opt, scanQuery);
    return pipe(
      actions(scanDB, this.opt, scanResultProcessor),
      toStream(this.opt.log),
    );
  }

  /* TODO: Add support for background job */
  /* processInBackground() {} */
}
