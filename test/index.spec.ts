import { actions, scanAction, updateAction, UpdateQuery } from '../src';

const opt = {
  throttle: 100,
  scanLimit: 2,
  log: {
    info: jest.fn(),
  },
};
const scanQuery = {
  FilterExpression: 'attribute_not_exists(objectId)',
};

describe('Scan and Update - ', () => {
  const dynamoClientMock: any = {
    scan: jest.fn(() => {
      return 'database data scaned';
    }),
    update: jest.fn(() => {
      return 'database data updated';
    }),
  };

  it('should scan database without last Evaluated Key', async () => {

    expect(await scanAction(dynamoClientMock, 'scan-table', opt, scanQuery)(undefined)).toMatchSnapshot();
    expect(dynamoClientMock.scan).toBeCalledWith({
      TableName: 'scan-table',
      FilterExpression: 'attribute_not_exists(objectId)',
      ConsistentRead: true,
      Limit: opt.scanLimit,
    }, opt);
  });

  it('should scan database from last Evaluated Key', async () => {
    expect(await scanAction(dynamoClientMock, 'scan-table', opt, scanQuery)({
      secondScan: true,
    })).toMatchSnapshot();
    expect(dynamoClientMock.scan).toBeCalledWith({
      TableName: 'scan-table',
      FilterExpression: 'attribute_not_exists(objectId)',
      ConsistentRead: true,
      Limit: opt.scanLimit,
      ExclusiveStartKey: {
        secondScan: true,
      },
    }, opt);
  });

  it('should update data', async () => {
    const conversationId = 'conversationId-A';
    const commentId = 'commentId-A';
    const containerId = 'containerId-A';
    const tableName = 'update-table';
    const query = {
      Key: {
        conversationId,
        commentId,
      },
      UpdateExpression: 'set objectId = :obid',
      ReturnValues: 'ALL_NEW',
      ExpressionAttributeValues: {
        ':obid': containerId,
      },
      TableName: tableName,
    };
    const updateQuery = {
      Key: {
        conversationId,
        commentId,
      },
      UpdateExpression: 'set objectId = :obid',
      ReturnValues: 'ALL_NEW',
      ExpressionAttributeValues: {
        ':obid': containerId,
      },
    };
    expect(await updateAction(dynamoClientMock, tableName, opt, updateQuery as UpdateQuery)).toMatchSnapshot();
    expect(dynamoClientMock.update).toBeCalledWith(query, opt);
  });
});

describe('Action - ', () => {
  const dynamoClientMock: any = {
    scan: jest.fn(),
    update: jest.fn(() => {
      return 'database data updated';
    }),
  };

  dynamoClientMock.scan.mockImplementationOnce(() => {
    return {
      LastEvaluatedKey: {
        cursor: 'last cursor',
      },
      Items: [
        {
          conversationId: 'CONV-A',
          commentId: 'COMM-A',
          containerId: 'CON-A',
        },
        {
          conversationId: 'CONV-B',
          commentId: 'COMM-B',
          containerId: 'CON-B',
        },
      ],
    };
  });
  dynamoClientMock.scan.mockImplementationOnce(() => {
    return {
      Items: [
        {
          conversationId: 'CONV-C',
          commentId: 'COMM-C',
          containerId: 'CON-C',
        },
      ],
    };
  });

  const updateQuery = {
    Key: {
      conversationId: '',
      commentId: '',
    },
    UpdateExpression: '',
    ReturnValues: '',
    ExpressionAttributeValues: {
      ':obid': '',
    },
  };

  it('should return and update all scaned items', async () => {
    const scan = scanAction(dynamoClientMock, 'action-table', opt, scanQuery);

    const scanResultProcessor = async (scanedValue: any) => {
      return await Promise.all(scanedValue.Items!.map(async (currentItem: any) => {
        const { conversationId, commentId, containerId } = currentItem;
        await updateAction(dynamoClientMock, 'action-table', opt, updateQuery as UpdateQuery);
        return await ({ commentId, conversationId, containerId });
      }));
    };
    for await (const r of await actions(scan, opt, scanResultProcessor)) {
      expect(r).toMatchSnapshot();
    }
    expect(dynamoClientMock.update).toHaveBeenCalledTimes(3);
  });
});
