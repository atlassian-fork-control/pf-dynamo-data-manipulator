# pf-dynamo-data-manipulator

A library for manipulator dynamoDB data.

## Install

```bash
npm install @atlassian/pf-dynamo-data-manipulator --save
```

## Requirements

- Node.js >= 8.0
- Npm >=6

## Usage
```
import DynamoManipulator, { ScanResult } from '@atlassian/pf-dynamo-data-manipulator';

/* Dynamo config */
const dynamoConfig = {
  apiVersion: '2012-08-10',
  endpoint: 'http://localhost:8080',
  region: 'localhost',
};
const tableName = 'a-table';

/*
  You can specify throttle and number of scan item in each interation
  this is to avoid dynamoDB reach read/write capacity
  By default, throttle is 5000 and scanLimit is 20
*/
const opt = {
  throttle: 5000,
  scanLimit: 50,
};

const manipulator = new DynamoManipulator(dynamoConfig, tableName, metrics, opt);

/* Create a function to add extra logic to update records */
const scanResultProcessor = async (scanedValue: ScanResult) => {
  return await Promise.all(scanedValue.Items!.map(async (currentItem) => {
    const { A, B, C } = currentItem;
    /* Construct update query based on scanned result */
    const updateQuery = {
      Key: { A, B },
      UpdateExpression: 'set D = :d',
      ExpressionAttributeValues: {
        ':d': C,
      },
      ReturnValues: 'ALL_NEW',
    };
    return await manipulator.updator(updateQuery);
    /* You can return anything here, need to be a promise, will be logged and streamed */
  }));
};

/* Pre-define a scan query */
const scanQuery = {
  FilterExpression: 'attribute_not_exists(D)',
};

/* Start the process and stream out the data in response */
ctx.type = 'text/event-stream';
ctx.status = 200;
ctx.body = manipulator.streamData(scanQuery, scanResultProcessor);

```